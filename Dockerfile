FROM debian:stable

RUN apt update && apt install -y \
	python3 \
	python3-pip \
    make gcc libfftw3-dev liblapacke-dev libpng-dev libopenblas-dev wget git unzip

ARG NB_USER="jovyan"
ARG NB_UID="1000"
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}

USER root
COPY requirements.txt ${HOME}
COPY jupyterlite-requirements.txt ${HOME}
RUN pip install --break-system-packages --upgrade --no-cache-dir jupyterhub notebook jupyterlab
RUN pip install --break-system-packages -r requirements.txt
RUN pip install --break-system-packages -r jupyterlite-requirements.txt
COPY . ${HOME}
RUN chown -R ${NB_UID} ${HOME}
RUN chmod -R 777 /opt/
USER ${NB_USER}

EXPOSE 8888

#CMD ["jupyter" , "notebook" , "--ip='0.0.0.0'" , "--port=8888", "--NotebookApp.token='crc1456livedoc'"]
CMD ["jupyter-lab" , "--ip='0.0.0.0'" , "--port=8888", "--NotebookApp.token='crc1456livedoc'"]
	
